FROM alpine:3.6
LABEL maintainer="Indexyz <jiduye@gmail.com>"

RUN echo "@testing http://nl.alpinelinux.org/alpine/edge/testing" >> /etc/apk/repositories && \
    # apk add --virtual=build-dep --no-cache g++ gcc libffi-dev libressl-dev && \
    apk add --update --no-cache apk-tools && \
    apk add --no-cache ca-certificates curl openssl p7zip unrar unzip && \
    apk add --no-cache 	--repository "http://nl.alpinelinux.org/alpine/edge/main" libressl2.7-libcrypto libressl2.7-libssl boost-python boost-system && \
    apk add --update --no-cache bash py-pip deluge@testing && \
    rm -rf /var/cache/apk/* && \
    pip install --upgrade pip && \
    pip install --upgrade setuptools && \
    pip2 install incremental constantly packaging automat service_identity && \
    # Clean up, kill kill kill
    rm -rf /root/.cache && \
    mkdir /config && \
    # Run script, skip the COPY layer
    echo "#!/bin/sh" > /start && \
    echo "set -e" >> /start && \
    echo "deluged -c /config" >> /start && \
    echo "deluge-web -c /config" >> /start && \
    chmod +x /start

VOLUME /root/Downloads /config

CMD ["/start"]
