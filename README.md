# Docker Torrent
Branche: Master - Deluge

> Deluge is a lightweight, Free Software, cross-platform BitTorrent client.

# How to use
## Self build
```bash
git clone https://gitlab.com/Indexyz/docker-torrent-alpine.git
cd docker-torrent-alpine
docker build -t deluge .	
docker run --net host -d deluge
```

## Pull From GitLab Registry
```bash
docker run --net host -d \
    registry.gitlab.com/indexyz/docker-torrent-alpine
```
## Use Volume
```bash
docker run --net host -d \
    -v /data/deluge/config:/config \
    -v /data/deluge/downloads:/root/Downloads \
    registry.gitlab.com/indexyz/docker-torrent-alpine
```
Volume can keep your data storge in disk even if you removed the contaniter

Download data will storage in `/data/deluge/downloads`

And your config will in `/data/deluge/config`
## Access Web Panel
Visit `http://your-ip:8112` and use thr defalut password **deluge** to login

> Please change your password for safe

Enjoy it.
